## 1590 Release News

The 1590 pre-release builds have begun. 1590 Final has been delayed several months beyond its initial target of January 15, 2016. Please contact info@genworks.com for access to the pre-releases. New development builds will be available on a regular basis between now and final release. 

Preliminary release notes are [here](https://gitlab.common-lisp.net/gendl/gendl/blob/devo/patches/1590/merged.txt).

Note that the [new & improved glime.lisp](https://gitlab.common-lisp.net/gendl/gendl/blob/devo/emacs/glime.lisp) is available with this release, but not being loaded by default. To load it, first ensure you have the lastest version from [here](https://gitlab.common-lisp.net/gendl/gendl/blob/devo/emacs/glime.lisp), then do:

   `(load (compile-file ".../glime.lisp"))`

